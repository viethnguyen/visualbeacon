/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class edu_rutgers_winlab_visualbeacon_VisualBeaconWrapper */

#ifndef _Included_edu_rutgers_winlab_visualbeacon_VisualBeaconWrapper
#define _Included_edu_rutgers_winlab_visualbeacon_VisualBeaconWrapper
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     edu_rutgers_winlab_visualbeacon_VisualBeaconWrapper
 * Method:    start
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_edu_rutgers_winlab_visualbeacon_VisualBeaconWrapper_start
  (JNIEnv *, jobject);

/*
 * Class:     edu_rutgers_winlab_visualbeacon_VisualBeaconWrapper
 * Method:    parseImage
 * Signature: ([BII)Z
 */
JNIEXPORT jboolean JNICALL Java_edu_rutgers_winlab_visualbeacon_VisualBeaconWrapper_parseImage
  (JNIEnv *, jobject, jbyteArray, jint, jint);

/*
 * Class:     edu_rutgers_winlab_visualbeacon_VisualBeaconWrapper
 * Method:    stop
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_edu_rutgers_winlab_visualbeacon_VisualBeaconWrapper_stop
  (JNIEnv *, jobject);

/*
 * Class:     edu_rutgers_winlab_visualbeacon_VisualBeaconWrapper
 * Method:    click
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_edu_rutgers_winlab_visualbeacon_VisualBeaconWrapper_click
  (JNIEnv *, jobject);

/*
 * Class:     edu_rutgers_winlab_visualbeacon_VisualBeaconWrapper
 * Method:    ImageProcessing
 * Signature: (II[B[I)Z
 */
JNIEXPORT jboolean JNICALL Java_edu_rutgers_winlab_visualbeacon_VisualBeaconWrapper_ImageProcessing
  (JNIEnv *, jobject, jint, jint, jbyteArray, jintArray);

#ifdef __cplusplus
}
#endif
#endif
