LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

OPENCV_LIB_TYPE:=STATIC
include /Users/vietnguyen/Development/OpenCV-2.4.8-android-sdk/sdk/native/jni/OpenCV.mk

APP_STL         := stlport_static
LOCAL_MODULE    := visualbeaconjni
LOCAL_SRC_FILES := VisualBeacon.cpp
LOCAL_LDLIBS	+= -llog -ljnigraphics
LOCAL_CPPFLAGS	:=  -frtti -fexceptions

include $(BUILD_SHARED_LIBRARY)