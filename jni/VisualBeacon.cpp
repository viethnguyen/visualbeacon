#include <jni.h>
#include "opencv/cv.h"
#include "opencv/highgui.h"

#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc_c.h"

#include <android/log.h>
#include <android/bitmap.h>

#include "include/edu_rutgers_winlab_visualbeacon_VisualBeaconWrapper.h"

//IplImage* frame = 0;
//IplImage* unfiltered = 0;
using namespace std;
using namespace cv;
Mat * mCanny = NULL;

#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT void JNICALL Java_edu_rutgers_winlab_visualbeacon_VisualBeaconWrapper_start
  (JNIEnv *, jobject){

}

JNIEXPORT jboolean JNICALL Java_edu_rutgers_winlab_visualbeacon_VisualBeaconWrapper_parseImage
  (JNIEnv * env, jobject obj, jbyteArray ba, jint w , jint h){
	return 0;
//	jbyte* bytes = env->GetByteArrayElements(ba, 0);
//	if(!bytes) {
//		return 0;
//	}
//
//	if(!frame) {
//
//		frame = cvCreateImage(cvSize(w, h), IPL_DEPTH_8U, 3);
//		unfiltered = cvCreateImage(cvSize(w, h), IPL_DEPTH_8U, 1);
//	}
//
//	// Wenjia' YUV2BGR conversion with OpenCV
//    Mat myuv(h + h/2, w, CV_8UC1, (unsigned char*)bytes);
//
//
//    Mat mbgra(h, w, CV_8UC3);
//
//    cvtColor(myuv, mbgra, CV_YUV420sp2BGR, 3);
//
//
//    *frame = mbgra;
//
//
//    cvSaveImage("/sdcard/aaatest.png", frame);
//
//	env->ReleaseByteArrayElements(ba, bytes, JNI_COMMIT);
}

JNIEXPORT void JNICALL Java_edu_rutgers_winlab_visualbeacon_VisualBeaconWrapper_stop
  (JNIEnv *, jobject){

}

JNIEXPORT void JNICALL Java_edu_rutgers_winlab_visualbeacon_VisualBeaconWrapper_click
  (JNIEnv *, jobject){

}

JNIEXPORT jboolean JNICALL Java_edu_rutgers_winlab_visualbeacon_VisualBeaconWrapper_ImageProcessing
(  JNIEnv* env, jobject thiz,
    jint width, jint height,
    jbyteArray NV21FrameData, jintArray outPixels){
	  jbyte * pNV21FrameData = env->GetByteArrayElements(NV21FrameData, 0);
	  jint * poutPixels = env->GetIntArrayElements(outPixels, 0);

	  if ( mCanny == NULL )
	  {
	    mCanny = new Mat(height, width, CV_8UC1);
	  }

	  Mat mGray(height, width, CV_8UC1, (unsigned char *)pNV21FrameData);
	  Mat mResult(height, width, CV_8UC4, (unsigned char *)poutPixels);
	  IplImage srcImg = mGray;
	  IplImage CannyImg = *mCanny;
	  IplImage ResultImg = mResult;

	  cvCanny(&srcImg, &CannyImg, 80, 100, 3);
	  cvCvtColor(&CannyImg, &ResultImg, CV_GRAY2BGRA);

	  env->ReleaseByteArrayElements(NV21FrameData, pNV21FrameData, 0);
	  env->ReleaseIntArrayElements(outPixels, poutPixels, 0);
	  return true;
}



#ifdef __cplusplus
}
#endif
