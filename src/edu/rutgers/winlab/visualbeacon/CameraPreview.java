package edu.rutgers.winlab.visualbeacon;

import java.io.IOException;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;

public class CameraPreview extends ViewGroup implements Callback, Camera.PreviewCallback {

    private final String TAG = "CameraPreview";

    SurfaceView mSurfaceView;
    SurfaceHolder mHolder;
    Size mPreviewSize;
    List<Size> mSupportedPreviewSizes;
    Camera mCamera;
    
    /* Preview frame manipulate */
    private byte[] frameData = null;
    private int imageFormat;
    private int[] pixels;
    private Bitmap bitmap = null; 
    private boolean isProcessing = false;

	VisualBeaconWrapper mImageProc;
    Handler mHandler = new Handler(Looper.getMainLooper());

    CameraPreview(Context context) {
        super(context);

        mSurfaceView = new SurfaceView(context);
        addView(mSurfaceView);

        // Install a SurfaceHolder.Callback so we get notified when the
        // underlying surface is created and destroyed.
        mHolder = mSurfaceView.getHolder();
        mHolder.addCallback(this);
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        
        // init image processing code
        mImageProc = new VisualBeaconWrapper();
        mImageProc.start();
        
    }

    public void setCamera(Camera camera) {
        mCamera = camera;
        if (mCamera != null) {
            mSupportedPreviewSizes = mCamera.getParameters().getSupportedPreviewSizes();
            requestLayout();
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // We purposely disregard child measurements because act as a
        // wrapper to a SurfaceView that centers the camera preview instead
        // of stretching it.
        final int width = resolveSize(getSuggestedMinimumWidth(), widthMeasureSpec);
        final int height = resolveSize(getSuggestedMinimumHeight(), heightMeasureSpec);
        Log.d(TAG, "onMeasure: " + String.valueOf(width) + "," + String.valueOf(height));
        setMeasuredDimension(width, height);

        if (mSupportedPreviewSizes != null) {
            mPreviewSize = getBestSupportedSize(mSupportedPreviewSizes, width, height);
        }
        
        bitmap = Bitmap.createBitmap(mPreviewSize.width, mPreviewSize.height, Bitmap.Config.ARGB_8888);
        pixels = new int[mPreviewSize.width * mPreviewSize.height]; 
        
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        if (changed && getChildCount() > 0) {
            final View child = getChildAt(0);

            final int width = r - l;
            final int height = b - t;

            int previewWidth = width;
            int previewHeight = height;
            if (mPreviewSize != null) {
                previewWidth = mPreviewSize.width;
                previewHeight = mPreviewSize.height;
            }
            Log.e("mimo_debug", "mPreview.width" + previewWidth);
            Log.e("mimo_debug", "mPreview.height" + previewHeight);

            // Center the child SurfaceView within the parent.
            if (width * previewHeight > height * previewWidth) {
                final int scaledChildWidth = previewWidth * height / previewHeight;
                child.layout((width - scaledChildWidth) / 2, 0,
                        (width + scaledChildWidth) / 2, height);
            } else {
                final int scaledChildHeight = previewHeight * width / previewWidth;
                child.layout(0, (height - scaledChildHeight) / 2,
                        width, (height + scaledChildHeight) / 2);
            }
        }
    }

    public void surfaceCreated(SurfaceHolder holder) {
        // The Surface has been created, acquire the camera and tell it where
        // to draw.
        try {
            if (mCamera != null) {
                mCamera.setPreviewDisplay(holder);
            }
        } catch (IOException exception) {
            Log.e(TAG, "IOException caused by setPreviewDisplay()", exception);
            mCamera.release();
            mCamera = null;
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        // Surface will be destroyed when we return, so stop the preview.
        if (mCamera != null) {
           
            mCamera.stopPreview();
            mCamera.setPreviewCallback(null);
            mCamera.release();
            mCamera = null;
        }
    }


    private Size getOptimalPreviewSize(List<Size> sizes, int w, int h) {
        final double ASPECT_TOLERANCE = 0.1;
        double targetRatio = (double) w / h;
        if (sizes == null) return null;

        Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;

        int targetHeight = h;

        // Try to find an size match aspect ratio and size
        for (Size size : sizes) {
        	Log.d(TAG, String.valueOf(size.width) + " " + String.valueOf(size.height));
        	
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
            if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }

        // Cannot find the one match the aspect ratio, ignore the requirement
        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                	minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }
        return optimalSize;
    }

	private Size getBestSupportedSize(List<Size> sizes, int width, int height){
		Size bestSize = sizes.get(0);
		int largestArea = bestSize.width * bestSize.height;
		for(Size s: sizes){
			int area = s.width * s.height;
			if(area > largestArea){
				bestSize = s;
				largestArea = area;
			}
		}
		return bestSize;
	}
	
    //change preview size here for resolution
    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        // Now that the size is known, set up the camera parameters and begin
        // the preview.
        Camera.Parameters parameters = mCamera.getParameters();
        Log.i("mimo_debug", "Initial camera parameters: " + parameters.flatten()); //displays all camera parameters
        parameters.setPreviewSize(mPreviewSize.width, mPreviewSize.height);
      // parameters.setPreviewSize(320, 240);   			//will not work coz of change in aspect ratio
    //  parameters.setPreviewSize(mPreviewSize.width/4, mPreviewSize.height/4);  			
        requestLayout();

        imageFormat = parameters.getPreviewFormat();
        
        mCamera.setParameters(parameters);
        mCamera.startPreview();
    }

	@Override
	public void onPreviewFrame(byte[] data, Camera camera) {
	    // At preview mode, the frame data will push to here.
	    if (imageFormat == ImageFormat.NV21)
	    {
	      //We only accept the NV21(YUV420) format.
	      if ( !isProcessing )
	      {
	        frameData = data;
	        mHandler.post(DoImageProcessing);
	      }
	    }
	}
	
	private Runnable DoImageProcessing = new Runnable(){
		public void run(){
			isProcessing = true;
			mImageProc.ImageProcessing(mPreviewSize.width, mPreviewSize.height, frameData, pixels);
			
			bitmap.setPixels(pixels, 0, mPreviewSize.width, 0, 0, mPreviewSize.width, mPreviewSize.height);
			
			Canvas canvas = null;
			try{
				synchronized (mHolder) {
					canvas = mHolder.lockCanvas();
					canvas.drawBitmap(bitmap, 0, 0, null);
					
				} 
				} catch(Exception e){
					e.printStackTrace();
				}finally{
					mHolder.unlockCanvasAndPost(canvas);
				}
			
			isProcessing = false;
			
		
		}
	};
}
