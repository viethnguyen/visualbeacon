package edu.rutgers.winlab.visualbeacon;

import android.app.Activity;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.PreviewCallback;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;

public class VisualBeaconActivity extends Activity{
	private static final String TAG = "VisualBeaconActivity";
	
	//CameraPreview mPreview;
	MyCameraView mMyCameraPreview;
	Camera mCamera;
	VisualBeaconWrapper mImageProc;
	View mProgressContainer;

	
	byte[] frame;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_visual_beacon);
        //mPreview = new CameraPreview(this);
        mMyCameraPreview = new MyCameraView(this);
        FrameLayout framePreview = (FrameLayout) findViewById(R.id.visual_beacon_cameraPreview);
        //framePreview.addView(mPreview);
		framePreview.addView(mMyCameraPreview);
        
		mProgressContainer = findViewById(R.id.visual_beacon_camera_progressContainer);
		mProgressContainer.setVisibility(View.INVISIBLE);
		
        // Find the total number of cameras available
        int numberOfCameras = Camera.getNumberOfCameras();
        
        // Find the ID of the default camera
        CameraInfo cameraInfo = new CameraInfo();
        for(int i=0; i<numberOfCameras; i++) {
        	Camera.getCameraInfo(i, cameraInfo);
        	if(cameraInfo.facing == CameraInfo.CAMERA_FACING_BACK) {
        		mCamera = Camera.open(i);
        		break;
        	}
        }
        
        if(mCamera != null) {
        	//mPreview.setCamera(mCamera);
        	mMyCameraPreview.setCamera(mCamera);
        }
        
        // init image processing code
        mImageProc = new VisualBeaconWrapper();
        mImageProc.start();
	}
	
    @Override
    protected void onResume() {
        super.onResume();
        
        // Open the default i.e. the first rear facing camera.
        if(mCamera == null) {
        	mCamera = Camera.open();
        	//mPreview.setCamera(mCamera);
        	mMyCameraPreview.setCamera(mCamera);
        }
        
        setListeners();
    }
    
    @Override
    protected void onPause() {
        super.onPause();

        releaseCamera();
    }
    
    private void setListeners(){
    	//mCamera.setPreviewCallback(mPreview);
    	mCamera.setPreviewCallbackWithBuffer(mMyCameraPreview);
    }
    
    private void releaseCamera(){
    	// Because the Camera object is a shared resource, it's very
        // important to release it when the activity is paused.
        if (mCamera != null) {
        	//mPreview.setCamera(null);
        	mMyCameraPreview.setCamera(null);
            mCamera.setPreviewCallback(null);
            mCamera.stopPreview();

            mCamera.release();
            mCamera = null;
        }
    }
	
}
