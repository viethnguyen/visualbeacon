package edu.rutgers.winlab.visualbeacon;

import java.io.IOException;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;

/**
 * Custom camera view, which inherits SurfaceView, allows redrawing on Surface based on Camera Preview frames
 * @author vietnguyen
 *
 */
public class MyCameraView extends SurfaceView implements Callback,
		PreviewCallback {
	private static final String TAG = "MyCameraView";

	private static final int BITMAP_MAX_WIDTH = 2000;
	private static final int BITMAP_MAX_HEIGHT = 1500;
	private Camera mCamera;
	private byte[] mVideoSource;
	private Bitmap mBackBuffer;
	private Paint mPaint;
	VisualBeaconWrapper mImageProc;

	Size mPreviewSize;
    List<Size> mSupportedPreviewSizes;
    private int[] pixels;

    PixelFormat mPixelFormat = new PixelFormat();
	
	public MyCameraView(Context context) {
		super(context);
		getHolder().addCallback(this);
		setWillNotDraw(false);
		
		// Init beforehands...
		mImageProc = new VisualBeaconWrapper();
		mBackBuffer = Bitmap.createBitmap(BITMAP_MAX_WIDTH, BITMAP_MAX_HEIGHT, Bitmap.Config.ARGB_8888);
        mPixelFormat = new PixelFormat();
	}

	@Override
	public void onPreviewFrame(byte[] pData, Camera pCamera) {
		pixels = new int[mPreviewSize.width * mPreviewSize.height];
		mImageProc.ImageProcessing(mPreviewSize.width, mPreviewSize.height, pData, pixels);
		
		mBackBuffer.setPixels(pixels, 0, mPreviewSize.width, 0, 0, mPreviewSize.width, mPreviewSize.height);
		
		invalidate();

	}

	@Override
	protected void onDraw(Canvas canvas) {
		if (mCamera != null) {
			canvas.drawBitmap(mBackBuffer, 0, 0, mPaint);
			mCamera.addCallbackBuffer(mVideoSource);
		}
	}
	
	
	@Override
	public void surfaceChanged(SurfaceHolder pHolder, int pFormat, int pWidth, int pHeight) {
		Camera.Parameters lParameters = mCamera.getParameters();
		lParameters.setPreviewSize(mPreviewSize.width, mPreviewSize.height);
		lParameters.setPreviewFormat(PixelFormat.YCbCr_420_SP);
		mCamera.setParameters(lParameters);
		mCamera.addCallbackBuffer(mVideoSource);
		mCamera.startPreview();

	}
	
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        final int width = resolveSize(getSuggestedMinimumWidth(), widthMeasureSpec);
        final int height = resolveSize(getSuggestedMinimumHeight(), heightMeasureSpec);
        setMeasuredDimension(width, height);

        if (mSupportedPreviewSizes != null) {
            mPreviewSize = getBestSupportedSize(mSupportedPreviewSizes, width, height);
        }

        Log.d(TAG, "onMeasure: " + String.valueOf(mPreviewSize.width) + "," + String.valueOf(mPreviewSize.height));
        
        //mBackBuffer = Bitmap.createBitmap(mPreviewSize.width, mPreviewSize.height, Bitmap.Config.ARGB_8888);
        pixels = new int[mPreviewSize.width * mPreviewSize.height]; 
		
		PixelFormat.getPixelFormatInfo(mCamera.getParameters().getPreviewFormat(), mPixelFormat);
		int lSourceSize = mPreviewSize.width * mPreviewSize.height* mPixelFormat.bitsPerPixel / 8;
		mVideoSource = new byte[lSourceSize];
	}

	private Size getBestSupportedSize(List<Size> sizes, int width, int height){
		Size bestSize = sizes.get(0);
		int largestArea = bestSize.width * bestSize.height;
		for(Size s: sizes){
			int area = s.width * s.height;
			if(area > largestArea){
				bestSize = s;
				largestArea = area;
			}
		}
		return bestSize;
	}
	
	private Size findBestResolution(int pWidth, int pHeight) {
		List<Size> lSizes = mCamera.getParameters()
		.getSupportedPreviewSizes();
		Size lSelectedSize = mCamera.new Size(0, 0);
		for (Size lSize : lSizes) {
			if ((lSize.width <= pWidth)
					&& (lSize.height <= pHeight)
					&& (lSize.width >= lSelectedSize.width)
					&& (lSize.height >= lSelectedSize.height)) {
				lSelectedSize = lSize;
			}
		}
		if ((lSelectedSize.width == 0)
				|| (lSelectedSize.height == 0)) {
			lSelectedSize = lSizes.get(0);
		}
		return lSelectedSize;
	}
	
	public void setCamera(Camera camera){
		mCamera = camera;
		if(mCamera!=null){

            mSupportedPreviewSizes = mCamera.getParameters().getSupportedPreviewSizes();
		}
	}

	@Override
	public void surfaceCreated(SurfaceHolder arg0) {
		try {
//			mCamera = Camera.open();
//			mCamera.setDisplayOrientation(0);
//			mCamera.setPreviewDisplay(null);
//			mCamera.setPreviewCallbackWithBuffer(this);
            if (mCamera != null) {
                mCamera.setPreviewDisplay(this.getHolder());
            }
		} catch (IOException eIOException) {
			mCamera.release();
			mCamera = null;
			throw new IllegalStateException();
		}

	}

	@Override
	public void surfaceDestroyed(SurfaceHolder arg0) {
		if (mCamera != null) {
			mCamera.stopPreview();
			mCamera.release();
			mCamera = null;
			mVideoSource = null;
			mBackBuffer = null;
		}

	}

}
