package edu.rutgers.winlab.visualbeacon;

import android.graphics.Bitmap;

public class VisualBeaconWrapper {
	static {
		System.loadLibrary("visualbeaconjni");
	}
	
	public native void start();
	
	public native boolean parseImage(byte[] image, int w, int h);
	
	public native void stop();
	public native void click();
	
	public native boolean ImageProcessing(int width, int height, 
		      byte[] NV21FrameData, int [] pixels);
	
	
}
